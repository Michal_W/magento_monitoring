<?php

class DBTeam_Adminloginfailed_FailedloginController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction() {
        $this->_title($this->__('Admin failed login'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('dbteam_adminloginfailed/adminhtml_adminloginfailed'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('dbteam_adminloginfailed/adminhtml_adminloginfailed_grid')->toHtml()
        );
    }
    public function customerAction() {
        $this->_title($this->__('Customer failed login'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()
            ->createBlock('dbteam_adminloginfailed/adminhtml_customerfailedlogin')->setTemplate('monitoring/customerfailedlogin.phtml'));
        $this->renderLayout();
    }
}