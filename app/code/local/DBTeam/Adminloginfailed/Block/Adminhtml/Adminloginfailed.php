<?php

class DBTeam_Adminloginfailed_Block_Adminhtml_Adminloginfailed extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'dbteam_adminloginfailed';
        $this->_controller = 'adminhtml_adminloginfailed';
        $this->_headerText = Mage::helper('dbteam_adminloginfailed')->__('Admin failed login');
        parent::__construct();
        $this->_removeButton('add');
    }
}