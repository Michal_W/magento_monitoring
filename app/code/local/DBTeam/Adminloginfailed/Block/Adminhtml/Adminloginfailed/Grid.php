<?php
class DBTeam_Adminloginfailed_Block_Adminhtml_Adminloginfailed_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('dbteam_adminloginfailed_grid');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $this->_collection = Mage::getModel('dbteam_adminloginfailed/loginfailed')->getCollection();
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('dbteam_adminloginfailed');

        $this->addColumn('failedlogin_id', array(
            'header' => $helper->__('ID'),
            'index'  => 'failedlogin_id'
        ));


        $this->addColumn('username', array(
            'header' => $helper->__('Username'),
            'index'  => 'username'
        ));


        $this->addColumn('date', array(
            'header' => $helper->__('Date'),
            'index'  => 'date',
            'type' => 'datetime'
        ));

        $this->addColumn('ip', array(
            'header' => $helper->__('IP Address'),
            'index'  => 'ip'
        ));


        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}