<?php

class DBTeam_Adminloginfailed_Block_Adminhtml_Customerfailedlogin extends Mage_Core_Block_Template
{
    public function getCustomerFailedLoginDays()
    {
        $days = Array();
        $daysLabels = Array();

        if (!file_exists('./var/log/failedlogin.log')) {
            file_put_contents('./var/log/failedlogin.log', '');
            chmod('./var/log/failedlogin.log', 0640);
        }

        $file = fopen(getcwd() . '/var/log/failedlogin.log', 'r');
        if ($file) {
            while (($line = fgets($file)) !== false) {
                array_push($days, substr($line, 0, 10));
            }

            fclose($file);
        }

        for ($i = 0; $i < sizeof($days); $i++) {
            if ($i == 0) {
                array_push($daysLabels, $days[$i]);
            } else {
                if ($days[$i] != $days[$i - 1]) {
                    array_push($daysLabels, $days[$i]);
                }
            }
        }

        $daysCount = Array();
        $i = 0;
        foreach ($daysLabels as $dayLabel) {
            $daysCount[$i]['day'] = $dayLabel;
            $daysCount[$i]['count'] = 0;
            $i++;
        }
        foreach ($days as $day) {
            foreach($daysCount as &$dayCount) {
                if($day==$dayCount['day']) {
                    $dayCount['count']++;
                }
            }
        }
        if(sizeof($daysCount)>14) {
            $daysCount=array_slice($daysCount,-14,14,true);
        }
        return $daysCount;

    }
}  