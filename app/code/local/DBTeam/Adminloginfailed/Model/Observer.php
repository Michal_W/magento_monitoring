<?php
class DBTeam_Adminloginfailed_Model_Observer
{
    public function Loginfailed(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $username = $event->getUserName();
        $write_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $fields_arr = array();
        $fields_arr['username'] = $username;
        $fields_arr['date'] = date("Y-m-d H:i:s");
        $fields_arr['ip'] = Mage::helper('core/http')->getRemoteAddr();
        $prefix = Mage::getConfig()->getTablePrefix();
        if ($prefix[0]!=null) {
            $write_connection->insert($prefix[0].'dbteam_loginfailed', $fields_arr);
        } else {
            $write_connection->insert('dbteam_loginfailed', $fields_arr);
        }
        return $this;
    }
}