<?php

class DBTeam_Adminloginfailed_Model_Resource_Loginfailed_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('dbteam_adminloginfailed/loginfailed');
    }
}