<?php

class DBTeam_Adminloginfailed_Model_Resource_Loginfailed extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('dbteam_adminloginfailed/loginfailed', 'failedlogin_id');
    }
}