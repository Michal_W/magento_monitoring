<?php
// app/code/local/Envato/Customapimodule/Model/Product/Api.php
class DBTeam_Adminloginfailed_Model_Failedlogins_Api extends Mage_Api_Model_Resource_Abstract
{
    public function getCustomerFailedLoginDays()
    {
        $days = Array();
        $daysLabels = Array();
        $file = fopen(getcwd() . '/var/log/failedlogin.log', 'r');
        if ($file) {
            while (($line = fgets($file)) !== false) {
                array_push($days, substr($line, 0, 10));
            }

            fclose($file);
        }

        for ($i = 0; $i < sizeof($days); $i++) {
            if ($i == 0) {
                array_push($daysLabels, $days[$i]);
            } else {
                if ($days[$i] != $days[$i - 1]) {
                    array_push($daysLabels, $days[$i]);
                }
            }
        }

        $daysCount = Array();
        foreach ($daysLabels as $dayLabel) {
            if (!isset($daysCount[$dayLabel])) {
                $daysCount[$dayLabel] = 1;
            } else {
                $daysCount[$dayLabel]++;
            }

        }
        
        if(sizeof($daysCount)>14) {
            $daysCount=array_slice($daysCount,-14,14,true);
        }
        return $daysCount;

    }
    
    public function getAdminFailedLogins() {
        $failedArray = array();

        $failedlogins = Mage::getModel('dbteam_adminloginfailed/loginfailed')->getCollection();
        foreach ($failedlogins as $failed) {
            array_push($failedArray, array('username' => $failed->getUsername(),
                'date' => $failed->getDate(),'ip' => $failed->getIp()));
        }
        return $failedArray;
    }
    
}