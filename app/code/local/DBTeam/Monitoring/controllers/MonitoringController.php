<?php

class DBTeam_Monitoring_MonitoringController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction() {
        $this->_title($this->__('DBTeam Monitoring'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()
            ->createBlock('dbteam_monitoring/fileslisting')
            ->setTemplate('monitoring/fileslisting.phtml'));
        $this->_addContent($this->getLayout()
            ->createBlock('dbteam_monitoring/lastlogin')
            ->setTemplate('monitoring/lastlogin.phtml'));
        
        $this->renderLayout();
    }

    public function ordersAction() {
        $this->_title($this->__('DBTeam Monitoring'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()
            ->createBlock('dbteam_monitoring/orders')
            ->setTemplate('monitoring/ordersstatus.phtml'));
        $this->renderLayout();
    }

    public function abandonedAction() {
        $this->_title($this->__('Abandoned carts'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('dbteam_monitoring/abandonedcarts'));
        $this->_addContent($this->getLayout()
            ->createBlock('dbteam_monitoring/abandonedcarts')
            ->setTemplate('monitoring/abandoned.phtml'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('dbteam_monitoring/abandonedcarts_grid')->toHtml()
        );
    }

    /**
     * @return array
     */
    public function pageloadingAction() {
        $this->_title($this->__('Page loading times:'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()
            ->createBlock('dbteam_monitoring/pageloading')
            ->setTemplate('monitoring/pageloading.phtml'));
        $this->renderLayout();
    }
}