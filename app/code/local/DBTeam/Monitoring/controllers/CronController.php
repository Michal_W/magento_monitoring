<?php

class DBTeam_Monitoring_CronController extends Mage_Adminhtml_Controller_Action
{
    

    public function cronAction() {
        $this->_title($this->__('Cron'));
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('dbteam_monitoring/cron'));
        $this->_addContent($this->getLayout()
           ->createBlock('dbteam_monitoring/cron')
            ->setTemplate('monitoring/cronjobs.phtml'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('dbteam_monitoring/cron_grid')->toHtml()
        );
    }
}