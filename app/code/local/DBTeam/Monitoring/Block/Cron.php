<?php

class DBTeam_Monitoring_Block_Cron extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'dbteam_monitoring';
        $this->_controller = 'cron';
        $this->_headerText = Mage::helper('dbteam_monitoring')->__('Cron schedule');
        parent::__construct();
        $this->_removeButton('add');
    }
    
    public function cronJobs() {
        $jobsArr = array();

        $jobs = Mage::getConfig()->getNode('crontab/jobs');
        foreach ($jobs as $schedule) {
            foreach ($schedule as $jobcode => $joba) {
                $jobsArr[$jobcode] = (string)$joba->schedule->cron_expr;
            }
        }
        return $jobsArr;
    }
}