<?php
class DBTeam_Monitoring_Block_Fileslisting extends Mage_Core_Block_Template
{
    const PATH = 'var/log';


    public function getFileslisting() {
        $filenames = array_diff(scandir(self::PATH), array('..', '.'));
        return $filenames;
  }


    public function getFilesSize() {
        $filesizes=array();
        $filenames = $this->getFileslisting();
        foreach($filenames as $filename) {
            array_push($filesizes,filesize(self::PATH.'/'.$filename));
        }
        return $filesizes;
    }
    public function getFilesTime() {
        $filestime=array();
        $filenames = $this->getFileslisting();
        foreach($filenames as $filename) {
            array_push($filestime, date("d M Y H:i:s", filemtime(self::PATH.'/'.$filename)));
        }
        return $filestime;
    }

    public function getReportListing() {
        if (!is_dir('./var/report')) {
            mkdir('./var/report');
            chmod('./var/report', 0750);
        }
        $filenames = array_diff(scandir('var/report'), array('..', '.'));
        return $filenames;
    }
    
    public function getReportTime() {
        $filestime=array();
        $filenames = $this->getReportListing();
        foreach($filenames as $filename) {
            array_push($filestime, date("d M Y H:i:s", filemtime('var/report/'.$filename)));
        }
        return $filestime;
    }
    
}