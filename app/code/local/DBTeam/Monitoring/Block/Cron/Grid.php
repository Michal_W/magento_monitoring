<?php
class DBTeam_Monitoring_Block_Cron_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('dbteam_monitoring_grid');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $this->_collection = Mage::getModel('cron/schedule')->getCollection();
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('dbteam_monitoring');

        $this->addColumn('job_code', array(
            'header' => $helper->__('job_code'),
            'index'  => 'job_code'
        ));


        $this->addColumn('status', array(
            'header' => $helper->__('status'),
            'index'  => 'status'
        ));

        $this->addColumn('messages', array(
            'header' => $helper->__('messages'),
            'index'  => 'messages'
        ));


        $this->addColumn('created_at', array(
            'header' => $helper->__('created_at'),
            'index'  => 'created_at',
            'type' => 'datetime'
        ));

        $this->addColumn('scheduled_at', array(
            'header' => $helper->__('scheduled_at'),
            'index'  => 'scheduled_at',
            'type' => 'datetime'
        ));

        $this->addColumn('executed_at', array(
            'header' => $helper->__('executed_at'),
            'index'  => 'executed_at',
            'type' => 'datetime'
        ));

        $this->addColumn('finished_at', array(
            'header' => $helper->__('finished_at'),
            'index'  => 'finished_at',
            'type' => 'datetime'
        ));

        $this->addColumn('reported_at', array(
            'header' => $helper->__('reported_at'),
            'index'  => 'reported_at',
            'type' => 'datetime'
        ));

        $this->addColumn('updated_at', array(
            'header' => $helper->__('updated_at'),
            'index'  => 'updated_at',
            'type' => 'datetime'
        ));



        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}