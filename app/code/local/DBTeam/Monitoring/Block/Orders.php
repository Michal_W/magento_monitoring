<?php
class DBTeam_Monitoring_Block_Orders extends Mage_Core_Block_Template {

    public $orders;

    public function __construct(){
        if($this->orders === null) {
            $this->orders = Mage::getModel('sales/order')->getCollection();
        }
    }

    public function getLastOrder()
    {
        $order = Mage::getModel('sales/order')->getCollection()->setOrder('increment_id', 'DESC')->getFirstItem();
        return $order->getCreatedAt();
    }

    public function getOrdersStatus()
    {
        return Mage::getModel('sales/order_status')->getResourceCollection()->getData();
    }

    public function getOrdersStatusCount()
    {
        $statuses = array();

        foreach ($this->orders as $order) {

            if(!isset($statuses[$order->getStatus()])) {
                $statuses[$order->getStatus()] = 1;
            } else {
                $statuses[$order->getStatus()]++;
            }
        }

        return $statuses;
    }
    public function getOrdersStatusTimeline($statuses) {
        
        $stat=Array();
        foreach ($statuses as $status => $value) {
            array_push($stat,$status);
        }

        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)]=array_fill_keys($stat, 0); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = array_fill_keys($stat, 0);
            }
        }


        foreach ($this->orders as $order) {
            $custDate = substr($order->getUpdatedAt(), 0, 10);
            $status = $order->getStatus();
            if($status != '') {
                $aryRange[$custDate][$status] = isset($aryRange[$custDate][$status]) ? $aryRange[$custDate][$status] + 1 : 0;
            }
        }



        return $aryRange;
    }


    public function getShippingMethodsCount()
    {
        $methodCounts = Array();
        foreach ($this->orders as $order) {

            if(!isset($methodCounts[$order->getShippingMethod()])) {
                $methodCounts[$order->getShippingMethod()] = 1;
            } else {
                $methodCounts[$order->getShippingMethod()]++;
            }
        }


        return $methodCounts;
    }

    public function getShippingMethodsTimeline() {
        
        $methodsArr=Array();

        foreach ($this->orders as $order) {
            if(array_search($order->getShippingMethod(),$methodsArr)===false) {
                array_push($methodsArr, $order->getShippingMethod());
            }
        }

        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)]=array_fill_keys($methodsArr, 0); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = array_fill_keys($methodsArr, 0);
            }
        }

        foreach ($this->orders as $order) {
            $custDate = substr($order->getCreatedAt(), 0, 10);
            $method = $order->getShippingMethod();
            $aryRange[$custDate][$method] = isset($aryRange[$custDate][$method] ) ? $aryRange[$custDate][$method] + 1 : 0;
        }
        


        return $aryRange;

    }

    public function getPaymentMethods()
    {
        return Mage::getSingleton('payment/config')->getActiveMethods();
    }

    public function getPaymentMethodsCount($payments)
    {
        $methodCounts = Array();
        
        foreach ($this->orders as $order) {
            if(!isset($methodCounts[$order->getPayment()->getMethod()])) {
                $methodCounts[$order->getPayment()->getMethod()] = 1;
            } else {
                $methodCounts[$order->getPayment()->getMethod()]++;
            }
        }

        return $methodCounts;
    }

    public function getPaymentMethodsTimeline($payments)
    {
        $paymentsArr = Array();
        foreach ($this->orders as $order) {
            if(array_search($order->getPayment()->getMethod(),$paymentsArr)===false) {
                array_push($paymentsArr, $order->getPayment()->getMethod());
            }
        }


        $aryRange = array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            $aryRange[date('Y-m-d', $iDateFrom)] = array_fill_keys($paymentsArr, 0); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                $aryRange[date('Y-m-d', $iDateFrom)] = array_fill_keys($paymentsArr, 0);
            }
        }

        foreach ($this->orders as $order) {
            $custDate = substr($order->getCreatedAt(), 0, 10);
            $payment = $order->getPayment()->getMethod();
            if($payment != '') {
                $aryRange[$custDate][$payment] = isset($aryRange[$custDate][$payment]) ? $aryRange[$custDate][$payment] + 1 : 0;
            }
        }

        return $aryRange;
    }

    public function getOrderDates() {
        

        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)] = 0; // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = 0;
            }
        }

        foreach ($this->orders as $order) {
            $orderDate = substr($order->getCreatedAt(), 0, 10);
            $aryRange[$orderDate] = isset($aryRange[$orderDate]) ? $aryRange[$orderDate] + 1 : 0;
        }
        

        return $aryRange;
    }
}
