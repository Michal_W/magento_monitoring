<?php
class DBTeam_Monitoring_Block_Lastlogin extends Mage_Core_Block_Template {



    public function getLastLogin() {

        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $query = 'SELECT login_at FROM '.$resource->getTableName('log/customer').' ORDER BY login_at DESC LIMIT 1';
        $lastVisited = $connection->fetchAll($query);
        return $lastVisited;
    }

    public function getCustomerLoginDates() {
        $dates = Array();
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $query = 'SELECT login_at FROM '.$resource->getTableName('log/customer');
        $collection = $connection->fetchAll($query);
        foreach($collection as $col) {
            
            
                if(!isset($dates[substr($col['login_at'], 0, 10)])) {
                    $dates[substr($col['login_at'], 0, 10)] = 1;
                } else {
                    $dates[substr($col['login_at'], 0, 10)]++;
                }
        }

        if(sizeof($dates)>30) {
            $dates=array_slice($dates,-30,30,true);
        }
        
        return $dates;
    }



    public function getLastRegistration() {
        $collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('created_at')->addAttributeToSort('created_at', 'DESC');
        $customer = $collection->getFirstItem();
        return $customer->getCreatedAt();
    }

    public function getLastOrder() {
        $order = Mage::getModel('sales/order')->getCollection()->setOrder('increment_id','DESC')->getFirstItem();
        return $order->getCreatedAt();
    }

    public function getRegistrationDates() {

        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)]=0; // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = 0;
            }
        }

        $customers = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('created_at');
        foreach ($customers as $customer) {
            $custDate = substr($customer->getCreatedAt(), 0, 10);
            $aryRange[$custDate] = isset($aryRange[$custDate] ) ? $aryRange[$custDate] + 1 : 0;
        }

        if(sizeof($aryRange)>30) {
            $aryRange=array_slice($aryRange,-30,30,true);
        }

        return $aryRange;

    }

    public function getEmailQueue() {

        $magentoVersion = Mage::getVersion();
        if (version_compare($magentoVersion, '1.9.1', '>=')) {

            $collection = Mage::getModel('core/email_queue')->getCollection()
                ->addOnlyForSendingFilter()
                ->setCurPage(1)
                ->load();
            return $collection->getItems();
        } else {
            return null;
        }
    }
    
    public function getEmailQueueDates($items) {
        $dates = Array();
        foreach ($items as $item) {
            

            if(!isset($dates[substr($item->getData('created_at'), 0, 10)])) {
                $dates[substr($item->getData('created_at'), 0, 10)] = 1;
            } else {
                $dates[substr($item->getData('created_at'), 0, 10)]++;
            }
            
        }
        
        if(sizeof($dates)>30) {
            $dates=array_slice($dates,-30,30,true);
        }

        return $dates;
    }

    public function getEmailTypes($items) {
        $types = Array();
        foreach ($items as $item) {
        

            if(!isset($types[$item->getData('event_type')])) {
                $types[$item->getData('event_type')] = 1;
            } else {
                $types[$item->getData('event_type')]++;
            }
        }

        return $types;
    }

    public function getFailurePayments()
    {
        $records = Array();

        if (!file_exists('./var/log/paymentfailture.log')) {
            file_put_contents('./var/log/paymentfailture.log', '');
            chmod('./var/log/paymentfailture.log', 0640);
        }

        $file = fopen(getcwd() . '/var/log/paymentfailture.log', 'r');
        if ($file) {
            while (($line = fgets($file)) !== false) {
                
                if(!isset($records[substr($line, 0, -1)])) {
                    $records[substr($line, 0, -1)] = 1;
                } else {
                    $records[substr($line, 0, -1)]++;
                }
            }

            


            return $records;

        }
    }


}