<?php
class DBTeam_Monitoring_Block_Abandonedcarts_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('dbteam_monitoring_grid');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $adapter = Mage::getSingleton('core/resource')->getConnection('sales_read');
        $minutes = 15;
        $from = $adapter->getDateSubSql(
            $adapter->quote(now()),
            $minutes,
            Varien_Db_Adapter_Interface::INTERVAL_MINUTE
        );
        $this->_collection = Mage::getResourceModel('sales/quote_collection')
            // ->addFieldToFilter('converted_at', $adapter->getSuggestedZeroDate())
            ->addFieldToFilter('updated_at', array('to' => $from));

        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('dbteam_monitoring');

        $this->addColumn('entity_id', array(
            'header' => $helper->__('ID'),
            'index'  => 'entity_id'
        ));


        $this->addColumn('items_qty', array(
            'header' => $helper->__('Items qty'),
            'index'  => 'items_qty'
        ));

        $this->addColumn('grand_total', array(
            'header' => $helper->__('Grand total'),
            'index'  => 'grand_total'
        ));


        $this->addColumn('updated_at', array(
            'header' => $helper->__('updated at'),
            'index'  => 'updated_at',
            'type' => 'datetime'
        ));
        
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}