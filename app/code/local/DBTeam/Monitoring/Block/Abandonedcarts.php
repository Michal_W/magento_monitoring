<?php

class DBTeam_Monitoring_Block_Abandonedcarts extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'dbteam_monitoring';
        $this->_controller = 'abandonedcarts';
        $this->_headerText = Mage::helper('dbteam_monitoring')->__('Abandoned carts');
        parent::__construct();
        $this->_removeButton('add');
    }
    
    public function abandonedDates() {
        $dates = Array();
        $adapter = Mage::getSingleton('core/resource')->getConnection('sales_read');
        $minutes = 15;
        $from = $adapter->getDateSubSql(
            $adapter->quote(now()),
            $minutes,
            Varien_Db_Adapter_Interface::INTERVAL_MINUTE
        );

        $collection = Mage::getResourceModel('sales/quote_collection')
            // ->addFieldToFilter('converted_at', $adapter->getSuggestedZeroDate())
            ->addFieldToFilter('updated_at', array('to' => $from))
            ->setOrder('updated_at','ASC');
        foreach ($collection as $col) {

            if(!isset($dates[substr($col->getData('updated_at'), 0, 10)])) {
                $dates[substr($col->getData('updated_at'), 0, 10)] = 1;
            } else {
                $dates[substr($col->getData('updated_at'), 0, 10)]++;
            }

        }

        if(sizeof($dates)>100) {
            $dates=array_slice($dates,-100,100,true);
        }
        return $dates;
    }

}