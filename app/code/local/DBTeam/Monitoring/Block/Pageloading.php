<?php
class DBTeam_Monitoring_Block_Pageloading extends Mage_Core_Block_Template {

    public function getPageLoadTimes() {
        $records = Array();

        if (!file_exists('./var/log/pageloading.log')) {
            file_put_contents('./var/log/pageloading.log', '');
            chmod('./var/log/pageloading.log', 0640);
        }

        $file = fopen(getcwd() . '/var/log/pageloading.log', 'r');
        if ($file) {
            while (($line = fgets($file)) !== false) {
                $records[] = explode(",",substr($line,0,-1));
            }

            fclose($file);

            $dates = Array();
            foreach($records as $record) {

                if(!isset($dates[$record[0]])) {
                    $dates[$record[0]] = array('category' => 0, 'product' => 0, 'homepage' => 0);
                }



            }

            foreach ($records as $record) {
                if ($dates[$record[0]][$record[1]] == 0) {
                    $dates[$record[0]][$record[1]] =(float) $record[2];
                } else if ($dates[$record[0]][$record[1]] > 0) {
                    $dates[$record[0]][$record[1]] = ($dates[$record[0]][$record[1]]+$record[2])/2 ;
                }
            }
            return $dates;
        }



    }
}