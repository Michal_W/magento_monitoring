<?php
// app/code/local/Envato/Customapimodule/Model/Product/Api.php
class DBTeam_Monitoring_Model_Monitoring_Api extends Mage_Api_Model_Resource_Abstract
{
    const PATH = 'var/log';


    public function getFilesListing()
    {
        $filenames = array_diff(scandir(self::PATH), array('..', '.'));
        $files = array();

        foreach($filenames as $file) {


            if(!isset($files[$file])) {
                $files[$file] = array('filesize' => filesize(self::PATH . '/' . $file), 'lastedit' => date("d M Y H:i:s",filemtime(self::PATH . '/' . $file)));
            }

        }



        $reportfilenames = array_diff(scandir('var/report'), array('..', '.'));
        $reports = array();

        foreach($reportfilenames as $reportfile) {
            $reports[filemtime('var/report/' . $reportfile)] = $reportfile;
        }

        krsort($reports);

        if(sizeof($reports)>10) {
            $reports=array_slice($reports,-10,10,true);
        }

        $reportsArr = array();
        foreach($reports as $key => $report) {
            $date = date("d M Y H:i:s", $key);
            $reportsArr[$date] = $report;
        }




        $array = array();
        array_push($array, $files);
        array_push($array, $reportsArr);

        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $query = 'SELECT login_at FROM '.$resource->getTableName('log/customer').' ORDER BY login_at DESC LIMIT 1';

        $array['lastlogin'] = $connection->fetchAll($query)[0];

        $collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('created_at')->addAttributeToSort('created_at', 'DESC');
        $customer = $collection->getFirstItem();
        $array['lastregister'] = $customer->getCreatedAt();

        $order = Mage::getModel('sales/order')->getCollection()->setOrder('increment_id','DESC')->getFirstItem();
        $array['lastorder'] = $order->getCreatedAt();

        return $array;

    }

    public function getRegistrations() {

        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)]=0; // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = 0;
            }
        }

        $customers = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('created_at');
        foreach ($customers as $customer) {
            $custDate = substr($customer->getCreatedAt(), 0, 10);
            $aryRange[$custDate] = isset($aryRange[$custDate] ) ? $aryRange[$custDate] + 1 : 0;
        }

        return $aryRange;

    }

    public function getLogins() {


        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)]=0; // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = 0;
            }
        }


        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('core_read');
        $query = 'SELECT login_at FROM '.$resource->getTableName('log/customer');
        $logins = $connection->fetchAll($query);
        foreach ($logins as $login) {
            $date = substr($login['login_at'], 0, 10);
            $aryRange[$date] = isset($aryRange[$date] ) ? $aryRange[$date] + 1 : 0;
        }

        return $aryRange;
    }

    public function getQueue() {

        $magentoVersion = Mage::getVersion();
        if (version_compare($magentoVersion, '1.9.1', '>=')) {

            $collection = Mage::getModel('core/email_queue')->getCollection()
                ->addOnlyForSendingFilter()
                ->setCurPage(1)
                ->load();
            $items = $collection->getItems();

            $dates = Array();
            foreach ($items as $item) {

                if(!isset($dates[substr($item->getData('created_at'), 0, 10)])) {
                    $dates[substr($item->getData('created_at'), 0, 10)] = 1;
                } else {
                    $dates[substr($item->getData('created_at'), 0, 10)]++;
                }
            }

            return $dates;
        } else {
            return null;
        }
    }

    public function getEmailTypes() {

        $magentoVersion = Mage::getVersion();
        if (version_compare($magentoVersion, '1.9.1', '>=')) {

            $collection = Mage::getModel('core/email_queue')->getCollection()
                ->addOnlyForSendingFilter()
                ->setCurPage(1)
                ->load();
            $items = $collection->getItems();

            $types = Array();
            foreach ($items as $item) {

                if(!isset($types[$item->getData('event_type')])) {
                    $types[$item->getData('event_type')] = 1;
                } else {
                    $types[$item->getData('event_type')]++;
                }

            }

            return $types;
        } else {
            return null;
        }
    }

    public function getFailedPayments()
    {
        $records = Array();

        $file = fopen(getcwd() . '/var/log/paymentfailture.log', 'r');
        if ($file) {
            while (($line = fgets($file)) !== false) {
                if(!isset($records[substr($line, 0, -1)])) {
                    $records[substr($line, 0, -1)] = 1;
                } else {
                    $records[substr($line, 0, -1)]++;
                }
            }
            return $records;

        }
    }

    public function getCronJobs() {
        $jobsArr = array();

        $jobs = Mage::getConfig()->getNode('crontab/jobs');
        foreach ($jobs as $schedule) {
            foreach ($schedule as $jobcode => $joba) {
                $jobsArr[$jobcode] = (string)$joba->schedule->cron_expr;
            }
        }
        return $jobsArr;
    }

    public function getCronSchedule() {
        $scheduleArr = array();
        $jobsArr = array();

         $jobs = Mage::getConfig()->getNode('crontab/jobs');
        foreach ($jobs as $schedule) {
            foreach ($schedule as $jobcode => $joba) {
                $jobsArr[$jobcode] = (string)$joba->schedule->cron_expr;
            }
        }

        $schedule = Mage::getModel('cron/schedule')->getCollection();
        foreach ($schedule as $job) {
            array_push($scheduleArr, array('jobcode' => $job->getData('job_code'), 'status' => $job->getData('status'),
                'messages' => $job->getData('messages'), 'createdat' => $job->getData('created_at'), 'scheduledat' => $job->getData('scheduled_at'),
                'executedat' => $job->getData('executed_at'), 'finishedat' => $job->getData('finished_at'), 'reportedat' => $job->getData('reported_at'),
                'updatedat' => $job->getData('updated_at'), 'jobexpr' => $jobsArr[$job->getData('job_code')]));
        }
        return $scheduleArr;
    }
}