<?php
class DBTeam_Monitoring_Model_Observer {

    public $time_start;

    public function timeStart(Varien_Event_Observer $observer){
        $this->time_start = microtime(true);
    }

    public function timeStop(Varien_Event_Observer $observer){
        $time_stop = microtime(true);
        $execution_time = ($time_stop - $this->time_start);
        $category = Mage::registry('current_category');
        $product  = Mage::registry('product');
        $isHomepage = 0;
        if (Mage::getSingleton('cms/page')->getIdentifier() == 'home') {
            $isHomepage = 1;
        }



        if ($product || $category || $isHomepage==1) {
            if($isHomepage==1) {
                $entity = 'homepage';
            } else if ($product) {
                $entity = 'product';
            } else {
                $entity = 'category';
            }
            if (!file_exists('./var/log/pageloading.log')) {
                file_put_contents('./var/log/pageloading.log', '');
                chmod('./var/log/pageloading.log', 0640);
            }

            $file = fopen(getcwd() . '/var/log/pageloading.log', 'a');
            fwrite($file,date("Y-m-d").",".$entity.",".round($execution_time,2)."\n");
            fclose($file);
        }
    }

    public function failedPayment(Varien_Event_Observer $observer) {
        $event = $observer->getEvent();

        if (!file_exists('./var/log/paymentfailture.log')) {
            file_put_contents('./var/log/paymentfailture.log', '');
            chmod('./var/log/paymentfailture.log', 0640);
        }

        $file = fopen(getcwd() . '/var/log/paymentfailture.log', 'a');
        fwrite($file,date("Y-m-d")."\n");
        fclose($file);
    }


}