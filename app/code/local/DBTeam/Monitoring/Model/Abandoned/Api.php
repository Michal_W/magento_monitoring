<?php

class DBTeam_Monitoring_Model_Abandoned_Api extends Mage_Api_Model_Resource_Abstract
{
    public function abandonedDates() {
        $dates = Array();
        $adapter = Mage::getSingleton('core/resource')->getConnection('sales_read');
        $minutes = 15;
        $from = $adapter->getDateSubSql(
            $adapter->quote(now()),
            $minutes,
            Varien_Db_Adapter_Interface::INTERVAL_MINUTE
        );

        $collection = Mage::getResourceModel('sales/quote_collection')
            // ->addFieldToFilter('converted_at', $adapter->getSuggestedZeroDate())
            ->addFieldToFilter('updated_at', array('to' => $from))
            ->setOrder('updated_at','ASC');

        foreach ($collection as $col) {
            if(!isset($dates[substr($col->getData('updated_at'), 0, 10)])) {
                $dates[substr($col->getData('updated_at'), 0, 10)] = 1;
            } else {
                $dates[substr($col->getData('updated_at'), 0, 10)]++;
            }
        }

        return $dates;
    }
    
    public function abandonedGrid() {
        $adapter = Mage::getSingleton('core/resource')->getConnection('sales_read');
        $minutes = 15;
        $from = $adapter->getDateSubSql(
            $adapter->quote(now()),
            $minutes,
            Varien_Db_Adapter_Interface::INTERVAL_MINUTE
        );
        $collection = Mage::getResourceModel('sales/quote_collection')
            ->addFieldToFilter('updated_at', array('to' => $from))->setOrder('updated_at','ASC');

        $abandoned = array();

        foreach ($collection as $row) {
            array_push($abandoned, array('itemsqty' => $row->getItemsQty(),'updatedat' => $row->getUpdatedAt(), 'customeremail' => $row->getCustomerEmail(),
                'itemscount' => $row->getItemsCount(), 'total' => $row->getGrandTotal()));
        }

        
        return $abandoned;

    }
}