<?php
// app/code/local/Envato/Customapimodule/Model/Product/Api.php
class DBTeam_Monitoring_Model_Orders_Api extends Mage_Api_Model_Resource_Abstract
{
    public $orders;

    public function __construct(){
        if($this->orders === null) {
            $this->orders = Mage::getModel('sales/order')->getCollection();
        }
    }

    public function ordersCount() {



        $ordersRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $ordersRange[date('Y-m-d',$iDateFrom)]=0; // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $ordersRange[date('Y-m-d',$iDateFrom)] = 0;
            }
        }


        foreach ($this->orders as $order) {
            $orderDate = substr($order->getCreatedAt(), 0, 10);
            $ordersRange[$orderDate] = isset($ordersRange[$orderDate] ) ? $ordersRange[$orderDate] + 1 : 0;
        }

        return $ordersRange;

    }


    public function getOrdersStatusTimeline() {
        $statuses=Mage::getModel('sales/order_status')->getResourceCollection()->getData();
        $orders = Mage::getModel('sales/order')->getCollection()->addAttributeToSort('updated_at', 'ASC');

        $stat=Array();
        foreach ($statuses as $status) {
            array_push($stat,$status['status']);
        }

        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)]=array_fill_keys($stat, 0); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = array_fill_keys($stat, 0);
            }
        }

        foreach ($orders as $order) {
            $custDate = substr($order->getUpdatedAt(), 0, 10);
            $status = $order->getStatus();
            if($status != '') {
                $aryRange[$custDate][$status] = isset($aryRange[$custDate][$status]) ? $aryRange[$custDate][$status] + 1 : 0;
            }
        }
        return $aryRange;
    }




    public function getShippingMethodsTimeline() {
        $methodsArr = array();

        foreach ($this->orders as $order) {
            if(array_search($order->getShippingMethod(),$methodsArr)===false) {
                array_push($methodsArr, $order->getShippingMethod());
            }
        }

        $aryRange=array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            $aryRange[date('Y-m-d',$iDateFrom)]=array_fill_keys($methodsArr, 0); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                $aryRange[date('Y-m-d',$iDateFrom)] = array_fill_keys($methodsArr, 0);
            }
        }

        foreach ($this->orders as $order) {
            $custDate = substr($order->getCreatedAt(), 0, 10);
            $method = $order->getShippingMethod();
            $aryRange[$custDate][$method] = isset($aryRange[$custDate][$method] ) ? $aryRange[$custDate][$method] + 1 : 0;
        }
        return $aryRange;

    }



    public function getPaymentMethodsTimeline() {

        $paymentsArr = Array();
        foreach ($this->orders as $order) {
            if(array_search($order->getPayment()->getMethod(),$paymentsArr)===false) {
                array_push($paymentsArr, $order->getPayment()->getMethod());
            }
        }


        $aryRange = array();
        $strDateFrom = '2013-01-01';
        $strDateTo = date('Y-m-d');

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            $aryRange[date('Y-m-d', $iDateFrom)] = array_fill_keys($paymentsArr, 0); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                $aryRange[date('Y-m-d', $iDateFrom)] = array_fill_keys($paymentsArr, 0);
            }
        }

        foreach ($this->orders as $order) {
            $custDate = substr($order->getCreatedAt(), 0, 10);
            $payment = $order->getPayment()->getMethod();
            if($payment != '') {
                $aryRange[$custDate][$payment] = isset($aryRange[$custDate][$payment]) ? $aryRange[$custDate][$payment] + 1 : 0;
            }
        }


        return $aryRange;
    }

}